CREATE TABLE IF NOT EXISTS addresses(
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    owner_id UUID NOT NULL REFERENCES customers(id),
    country varchar(50),
    street varchar(50)
);