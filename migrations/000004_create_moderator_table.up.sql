CREATE TABLE IF NOT EXISTS moderator(
    id UUID NOT NULL PRIMARY KEY,
    name TEXT,
    password TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);