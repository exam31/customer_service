CREATE TABLE IF NOT EXISTS customers(
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    first_name varchar(30),
    last_name varchar(30),
    bio text,
    email varchar(50),
    password text,
    phone_number varchar(15),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP,
    refresh_token text
);  