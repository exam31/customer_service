pull_submodule:
	git submodule update --init --recursive

update_submodule:	
	git submodule update --remote --merge
	
run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:customergroup1/protos.git

run_script:
	./script/gen-proto.sh

swag:
	swag init -g ./api/router.go -o api/docs

create_migrate:
	migrate create -ext sql -dir migrations -seq create_customers_table


create_table:
	migrate -source file://migrations -database postgres://postgres:Javohir_1@database-1.cxohj5uqpurt.ap-northeast-1.rds.amazonaws.com:5432/customerdata?sslmode=disable up

migrate_up:
	migrate -path migrations/ -database postgres://postgres:Javohir_1@database-1.cxohj5uqpurt.ap-northeast-1.rds.amazonaws.com:5432/customerdata up

migrate_down:
	migrate -path migrations/ -database postgres://postgres:Javohir_1@database-1.cxohj5uqpurt.ap-northeast-1.rds.amazonaws.com:5432/customerdata down
